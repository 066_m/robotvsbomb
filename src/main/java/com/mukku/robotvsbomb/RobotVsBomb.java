/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mukku.robotvsbomb;

/**
 *
 * @author Acer
 */
public class RobotVsBomb {
    private int N, xr, yr, xb, yb;
    private char lastDirection = ' ';
    
    public RobotVsBomb(int N, int xr, int yr, int xb, int yb){
        this.N = N;
        if(!inMap(xr,yr)){
            this.xr = 0;
            this.yr = 0;
        } else{
            this.xr = xr;
            this.yr = yr;
        }
        if(!inMap(xb,yb)){
            this.xb = 0;
            this.yb = 0;
        } else{
            this.xb = xb;
            this.yb = yb;
        }
        
    }
    
     public boolean inMap(int xr, int yr){
        if(xr < 0 || xr >= N || yr < 0 || yr >= N){
            return false;
        }
        return true;
    }
     
    public boolean isBomb(){
        if((xr == xb) && (yr == yb)){
            return true;
        }
        return false;
    }
    
    public boolean walk(char direction){
        switch(direction){
            case 'N' : 
                if(!inMap(xr, yr-1)){
                    tellUnableMove();
                    return false;
                }
                yr -= 1;
                break;
            case 'S' : 
                if(!inMap(xr, yr+1)){
                    tellUnableMove();
                    return false;
                }
                yr += 1;
                break;
            case 'E' : 
                if(!inMap(xr+1, yr)){
                    tellUnableMove();
                    return false;
                }
                xr += 1;
                break;
            case 'W' : 
                if(!inMap(xr-1, yr)){
                    tellUnableMove();
                    return false;
                }
                xr -= 1;
                break;
        }
        lastDirection = direction;
        if(isBomb()){
            tellFound();
        }
        return true;
    }
    
    public boolean walk(char direction, int step){
        for(int i = 0; i < step; i++){
            if(!this.walk(direction)) {
                return false;
            }  
        }
        return true;
    }
    
    public boolean walk(){
        return this.walk(lastDirection);
    }
    
    public boolean walk(int step){
        return this.walk(lastDirection, step);
    }
    
    @Override
    public String toString(){
        return "Now, I'm standing at (" + this.xr + ", " + this.yr + ") and distancing between bomb and me is ("
                + Math.abs(xr-xb) + ", " + Math.abs(yr-yb) +")";
    }
    
    public void tellUnableMove(){
        System.out.println("I can't move!!!");
    }
    
    public void tellFound(){
        System.out.println("Bomb found!!!");
    }
    
  /*  public void startXY(){
        double xr = 0;
        double yr = 0;
    }
    
    public void startXY(double xr, double yr){
        this.xr = xr;
        this.yr = yr;
    }
    
    public void Bomb(double xb, double yb){
        this.xb = xb;
        this.yb = yb;
    }
    
    public double calDistance(){
        return Math.sqrt(Math.pow((xb - xr), 2) + Math.pow((yb - yr), 2));
    }
    
    public void calStatus(){
        System.out.println("Now, I'm standing at (" + xr + "," + yr + ")");
        System.out.println("Distance between bomb and me is " + calDistance());
    }
    
    public void walkState(){
        if(xr == xb && yr == yb){
            System.out.println("Bomb found!!!");
            calStatus();
        } else if(xr >= N || xr < 0 || yr >= N || yr < 0){
            System.out.println("I can't move!!!");
            return;
        } else {
            calStatus();
        }
    }
    
    public void walk(char direction, double n){
        if(direction == 'N') yr-=n;
        else if(direction == 'S') yr+=n;
        else if(direction == 'E') xr+=n;
        else if(direction == 'W') xr-=n;
        walkState();
        this.xr = xr;
        this.yr = yr;
    }
   */ 
    
}
